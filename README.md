# 2180608413
|       Title    		| Search books |
|------------------------|-------------------------------|
|Value Statement	|As a customer, I want to find books quickly so I can view the books I want            |
|Acceptance Criteria        |  **Scenario 1** <br> Given that  to already knowthe book's information about the book's title, genre, and author and price .when customers enter information into the boxes and enter into the search box. Then outputs information about the book the customer is looking for|
| Definition of Done | <ul> <li>Unit Tests Passed</li> <li>Acceptance Criteria Met</li> <li>Code Reviewed</li> <li>Functional Tests Passed</li> <li>Non-Functional Requirements Met</li> <li>Product Owner Accepts User Story</li> </ul> |
|Owner          |Hoai Phuong| |
 | Interation: | Unscheduled 
 | Estimate: | 5 point

![searchbooks](/uploads/03fc693824064f1b9ea92506e4a059f5/searchbooks.png)

| Test CaseID  | Test Objective | Precondition|Steps|Test Data|Expected results|Post Condition |
| ------ | ------ |------ |------ |------ |------ |------ |
|  TC_01      |   Verify that the book search function works correctly and returns relevant results based on the user's input.    | Required to enter complete information      | 1. Click on the search icon on the top right corner of the screen.<br> 2. Enter a keyword or a phrase related to the book title, author, genre,  in the search box and press enter.<br> 3. Observe the results displayed on the screen.      |   a valid keywords  | The results should match the user's input and show the book details such as title, author, genre, price, and availability.      |       |


# 2180608203

|      Title          |   Show menu of books                                         ||
|----------------|-------------------------------|-----------------------------|
|Value Statement|        As a bookseller, I want to be able to view a menu that lists all the available books to the customer, so that they can easily browse and select the book they want to buy. |          |
|Acceptance Criteria          |Given the menu display a list of books, each book should be displayed with its title, author, and a brief description.<br>When the customers open app or click on the list of books. <br> Then a list with many book genres appears, this menu should be easy to navigate and scroll through and Clicking on a book should open a detailed view or page providing more information and option as adding it to wishlist or purchasing it              |      |     
|Definition of Done         |Unit Tests Passed <br> Acceptance Criteria Met <br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story||
|		Owner			|			Tran Thanh Tuong					|			|				
|			Iteration		|		Unscheduled							|			|			
|					Estimate|5 Points								||

![giaodien](/uploads/a9c1ccf218a90c609145ba3d1ecf1a3d/giaodien.png)
 
 # 2180603433

| Title: | FeedBack |
| :-------- | :----------------- |
| Value Statement: | As a customer, <br>I would like to be able to give feedback on the product, <br> my experience or submit store advice, to share your opinion and help improve the store's services.  |
| Acceptance Criteria: | Acceptance Criterion 1:  <br>Customers need to be notified of successful submission of their feedback or advice <br>All feedback must be stored in the store's database for future reference and analysis <br>If necessary, the store should have the ability to contact customers to request additional information or provide personal feedback <br> <br> <br>Acceptance Criterion 2: <br>If any changes or improvements are made to products or services based on customer feedback, the store should notify customers about this. <br>The store should have a clear policy on accepting or rejecting inappropriate or rule-violating feedback <br>The system needs to undergo performance testing to ensure that the process of providing feedback does not negatively impact the user experience <br> |
| Definition of Done: |  - Unit test passed <br> - Acceptance criteria are met <br> - The code has been evaluated <br> - Functional test passed <br> - Non-functional requirements have been met <br> - Save and rate tests |
| Owner: | Khang | Owner |
| Iteration: | Unscheduled |
| Estimate: | 5 Points |

![khang](/uploads/d2cc4059a42f770ed7c76a38851b614d/khang.png)

| Title: | Manage Books |
| ------ | ------ |
| Value Statement: | As a book manager, I want to update, delete and search books in the database, so that I can manage the inventory and availability of books. |
| Acceptance Criteria: | <ul> <li>The book manager can access a web page with a form to enter the book details (title, author, genre, ISBN, etc.)</li> <li>The book manager can submit the form and see a confirmation message that the book has been added to the database</li> <li>The book manager can view a list of all the books in the database with their details</li> <li>The book manager can edit or delete any book from the list by clicking on a button</li> <li>The book manager can search for a book by entering a keyword or a filter (e.g. genre, author, etc.)</li> </ul> |
| Definition of Done | <ul> <li>Acceptance Criteria Met</li> <li>Code reviewed</li> <li>Product owner Accepts User story</li> </ul> |
| Owner | Book Manager |
| Iteration | Unscheduled |
| Estimate | 5 Points |

# Main User Interface
![mainUS](/uploads/261ec79e1d9e3e3f8aea54bc6176e793/mainUS.png)

# Menu Items – Specific Books
![menuUS](/uploads/3ef3a9f683df0557e69e2413e3dea729/menuUS.png)

# Form Edit Book
![editUS](/uploads/11bb99ea15ee3a0017f7f9d67b217baf/editUS.png)

# Form Book Detail
![detailUS](/uploads/e9d0120996b4dd2d679da2620f126929/detailUS.png)

# Form Import Books
![importUS](/uploads/e12c4f0c8269e84f21b5b09098a95e84/importUS.png)


|       Title    		|Check out my cart and payment              
|------------------------|-------------------------------|
| Value | As a customer, I want to check out my cart and payment, so I can review the products I choose.|
|Acceptance Criteria	|1. Adding Items to Cart: Users can add products to the cart by clicking the "Add to Cart" button on product pages.<br> 2. Cart Interaction:  Users can easily change the quantity of items in the cart (increase/decrease/remove).<br>3. Shipping and Tax Calculation: The cart calculates shipping fees based on the user's selected shipping method and location.<br>4. Payment Information: Users can enter payment information securely, including credit card details or other payment methods.<br>5. Order Review: Users can review their delivery information and upon receipt can review the item.<br>6. Payment Processing: After confirming the order, the system processes the payment securely and efficiently.<br>7. Payment Gateway Integration: Can link with banks and other payment platforms, Payment gateway integration is thoroughly tested to ensure successful transaction processing.             |
|Definition of Done         |1.Functional Requirements.<br>2.User Acceptance Testing.<br>3.User Interface (UI/UX).<br>4.Data Management <br>5.Testing and Verification. |
|Owner          |Responsible person: Huynh Truong Hoan|
|Interation | Unscheduled|
|Estimate: | 5 Points |

![Untitled__3_](/uploads/1ade53127b892f71e93e82c77740d4d4/Untitled__3_.png)

![Untitled__4_](/uploads/f058e955baf1a3aa4078f42720dcf456/Untitled__4_.png)

![Untitled__5_](/uploads/b0a2f62275a20ac0b494518bc69c546b/Untitled__5_.png)
